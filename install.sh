#!/bin/bash
# A sample Bash script, by Ryan

rm -rf /var/www/html/*

cp -r DemandAVidYo/ /var/www/html/

cp DemandAVidYo/.env.server /var/www/html/DemandAVidYo/.env

cd /var/www/html/DemandAVidYo

composer install

touch /var/www/html/DemandAVidYo/database/database.sqlite

php artisan migrate
php artisan db:seed
