@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach ($videos as $video)


            <div class="card text-center mt-3">
              <div class="card-header">
                {{ $video->title }}
  </div>
  <div class="card-body">
    <img src="{{ asset('thumbnails/' . $video->thumbnail . '.jpg') }}" class="img-fluid ">

    <p class="card-text"><a href="{{ action('VideoController@watch', ['id' => $video->video_id]) }}" class="btn btn-primary  mt-3">Watch</a></p>
  </div>
  <div class="card-footer text-muted">

    @foreach($video->Countries as $country)

    <span class="badge badge-pill badge-primary">{{ $country->name }}</span>
    @endforeach


  </div>
</div>

            @endforeach
        </div>
    </div>
</div>
@endsection
