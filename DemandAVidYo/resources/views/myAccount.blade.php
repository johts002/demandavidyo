@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <form>
            <div class="form-group row">
              <label for="name" class="col-sm-2 col-form-label">Name</label>
              <div class="col-sm-10">
                <input type="text" readonly class="form-control-plaintext" id="name" value="{{ $user->name }}">
              </div>
            </div>
            <div class="form-group row">
              <label for="email" class="col-sm-2 col-form-label">Email</label>
              <div class="col-sm-10">
                <input type="text" readonly class="form-control-plaintext" id="email" placeholder="{{ $user->email}}">
              </div>
            </div>
            <div class="form-group row">
              <label for="country" class="col-sm-2 col-form-label">Country</label>
              <div class="col-sm-10">
                <input type="text" readonly class="form-control-plaintext" id="country" placeholder="{{ $user->country->name }}">
              </div>
            </div>
            <div class="form-group row">
              <label for="membership" class="col-sm-2 col-form-label">Membership</label>
              <div class="col-sm-10">
                <input type="text" readonly class="form-control-plaintext" id="membership" placeholder="{{ $user->membership->name }}">
              </div>
            </div>
          </form>
        </div>
    </div>
</div>
@endsection
