@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card text-center">
              <div class="card-header">
                {{$title}}
  </div>
  <div class="card-body">
    <h5 class="card-title">{{$message}}</h5>
    <p class="card-text"></p>
  </div>
  <div class="card-footer text-muted">




  </div>
</div>


        </div>
    </div>
</div>
@endsection
