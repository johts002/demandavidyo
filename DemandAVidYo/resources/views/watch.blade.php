@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card text-center">
              <div class="card-header">
                {{$video->title}}
  </div>
  <div class="card-body">
    <img src="{{ asset('thumbnails/' . $video->thumbnail . '.jpg') }}" class="img-fluid ">

  </div>
  <div class="card-footer text-muted">
    <p class="card-text"> {{ $video->description }}</p>



  </div>
</div>


        </div>
    </div>
</div>
@endsection
