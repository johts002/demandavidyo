<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'country_id', 'membership_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function membership()
    {
      return $this->belongsTo(Membership::class,'membership_id','membership_id');
    }

    public function country()
    {
      return $this->belongsTo(Country::class,'country_id','country_id');
    }

    public function localVideos()
    {
      return $this->hasManyThrough(Video::class,Country::class);
    }

    public function sessionLogs()
    {
      return $this->hasMany(SessionLog::class);
    }
}
