<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $primaryKey = "event_id";




    public function eventOccurances()
    {
      return $this->hasMany(EventOccurance::class);
    }

}
