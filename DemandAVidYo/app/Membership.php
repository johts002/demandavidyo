<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    //

    public $timestamps = false;
    protected $primaryKey = 'membership_id';


    public function Users()
    {
      $this->hasMany('App\Users','membership_id','membership_id');
    }

}
