<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //
    protected $primaryKey = "video_id";


    protected $fillable = [
      'location',
      'title',
      'description',
      'release_date',
      'thumbnail'
    ];



    public function countries()
    {
       return $this->belongsToMany(Country::class,'country_video');
    }

}
