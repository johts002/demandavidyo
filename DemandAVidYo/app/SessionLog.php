<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionLog extends Model
{
    //

    protected $primaryKey = 'session_log_id';

    protected $fillable = [
      'user_user_id',
      'session_id'
    ];


    protected $table = "session_logs";



    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function eventOccurances()
    {
      return $this->hasMany(EventOccurance::class);
    }
}
