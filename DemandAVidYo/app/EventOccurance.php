<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventOccurance extends Model
{
    //
    //protected $primaryKey = '';


    public function sessionlog()
    {
      return $this->belongsTo(SessionLog::class);
    }



    public function event()
    {
      return $this->belongsTo(Event::class);
    }




}
