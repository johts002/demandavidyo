<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    public $timestamps = false;
    protected $primaryKey = "country_id";




    public function Citizens()
    {
      return $this->hasMany('App\Users','country_id','country_id');
    }


    public function videos()
    {
      return $this->belongsToMany(Video::class,'country_video');
    }

}
