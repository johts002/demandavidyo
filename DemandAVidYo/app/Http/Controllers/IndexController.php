<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    //
    public function index()
    {
      $user = Auth::user();


      if(is_null($user))
      {
        return view('welcome');
      }
      else
      {
        return redirect()->route('home');
      }
    }
}
