<?php

namespace App\Http\Controllers\Auth;


use App\Event;
use App\SessionLog;
use App\User;
use App\EventOccurance;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
    logout as performLogout;
}

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    protected function authenticated(Request $request, $user)
    {
    // stuff to do after user logs in



    $session_id = Session::getId();
    $event = Event::where('code','=','LI')->first();

    $eventOccurance = new EventOccurance;


    //dd($eventOccurance,$event);

    $session = SessionLog::firstOrCreate([
      'session_id' => $session_id
    ]);


    $eventOccurance->event()->associate($event);
    $eventOccurance->sessionLog()->associate($session);
    $eventOccurance->save();


    $session->user()->associate($user);
    $session->save();

  }

  public function logout(Request $request)
{

$user = Auth::user();

  $session_id = Session::getId();
  $event = Event::where('code','=','LO')->first();

  $eventOccurance = new EventOccurance;


  //dd($eventOccurance,$event);

  $session = SessionLog::firstOrCreate([
    'session_id' => $session_id
  ]);


  $eventOccurance->event()->associate($event);
  $eventOccurance->sessionLog()->associate($session);
  $eventOccurance->save();


  $session->user()->associate($user);
  $session->save();


$this->performLogout($request);

    return redirect()->route('index');
}
}
