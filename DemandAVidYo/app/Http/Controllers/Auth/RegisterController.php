<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Country;
use App\Membership;
use App\SessionLog;
use App\Event;
use App\EventOccurance;
use Session;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {


        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'country' => 'required|string',
            'membership' => 'required|string'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {


      $country = Country::find($data['country']);
      $membership = Membership::find($data['membership']);


      if (is_null($country)) { $country = Country::find(1); };
      if (is_null($membership)){ $membership = Membership::find(1); };


        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);




        $user->membership()->associate($membership);
        $user->country()->associate($country);

        $user->save();



        $session_id = Session::getId();
        $event = Event::where('code','=','REG')->first();

        $eventOccurance = new EventOccurance;


        //dd($eventOccurance,$event);

        $session = SessionLog::firstOrCreate([
          'session_id' => $session_id,
        ]);


        $eventOccurance->event()->associate($event);
        $eventOccurance->sessionLog()->associate($session);
        $eventOccurance->save();


        $session->user()->associate($user);
        $session->save();






        return $user;



    }

    public function showRegistrationForm() {



    $countries = Country::all();
    $memberships = Membership::all();

    return view ('auth.register', [
      'countries'=>$countries,
      'memberships'=>$memberships
    ]);
}


}
