<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class MyAccountController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function myAccount()
    {
      $user = Auth::user();



      return view('myAccount', ['user' => $user]);
    }
}
