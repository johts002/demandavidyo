<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


use Session;

use App\Video;
use App\Country;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



        $user = Auth::user();

        $country = Country::find($user->country_id);

        $country_id  = $country->country_id;

        $videos = null;

        if ($user->membership_id == 1) {
            $videos = $country->videos()
          ->with('countries')
          ->where(['country_country_id' => $country_id])
          ->get();
        } else {
            $videos = Video::orderBy('release_date','desc')
            ->with('countries')
            ->paginate(10);
        }

        //dd($videos);

        return view('home', ['videos' => $videos]);
    }
}
