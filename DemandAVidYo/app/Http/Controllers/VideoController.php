<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\CountryVideo;
use App\Video;
use App\Event;
use App\EventOccurance;
use App\SessionLog;

use Session;

class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    //


    public function index()
    {
        return redirect()->route('home');
    }

    public function watch($id)
    {
        $user = Auth::user();

        $session_id = Session::getId();
        $event = Event::where('code', '=', 'WATCH')->first();

        $eventOccurance = new EventOccurance;


        //dd($eventOccurance,$event);

        $session = SessionLog::firstOrCreate([
          'session_id' => $session_id
        ]);


        $eventOccurance->event()->associate($event);
        $eventOccurance->sessionLog()->associate($session);
        $eventOccurance->save();


        $session->user()->associate($user);
        $session->save();


        $video = Video::find($id);

        if (is_null($video)) {
            return view(
              'videoerror',
              [
                'message'=> 'The video you are looking for does not exist',
                'title' => 'Video Not Found'
              ]
            );
        }


        $user = Auth::user();
        $country_id = $user->country_id;
        $membership_id = $user->membership_id;


        if ($membership_id == 1) {
          $countryVideo = CountryVideo::where([
            'country_country_id' => $country_id,
            'video_video_id' => $id
          ])
          ->get();

          if (count($countryVideo) == 0) {
                return view(
                  'videoerror',
                  [
                    'message'=> 'You do not have permission to view this video',
                    'title' => 'Please update your subscription'
                  ]
                );
          }




        }
        return view('watch', ['video' => $video]);

    }
}
