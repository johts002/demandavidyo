<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignContraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('users', function (Blueprint $table) {

          $table->unsignedInteger('membership_id')->nullable();
          $table->unsignedInteger('country_id')->nullable();

            $table->foreign('country_id')->references('country_id')->on('countries');
            $table->foreign('membership_id')->references('membership_id')->on('memberships');
          });


          Schema::table('country_video', function (Blueprint $table) {

            $table->unsignedInteger('video_video_id')->nullable();
            $table->unsignedInteger('country_country_id')->nullable();

              $table->foreign('country_country_id')->references('country_id')->on('countries');
              $table->foreign('video_video_id')->references('video_id')->on('videos');
            });


            Schema::table('session_logs', function (Blueprint $table) {
              $table->unsignedInteger('user_user_id')->nullable();

                $table->foreign('user_user_id')->references('user_id')->on('users');

            });

            Schema::table('event_occurances', function (Blueprint $table) {
              $table->unsignedInteger('event_event_id')->nullable();
              $table->unsignedInteger('sessionlog_session_log_id')->nullable();

                $table->foreign('event_event_id')->references('event_id')->on('events');
                $table->foreign('sessionlog_session_log_id')->references('session_log_id')->on('session_logs');

            });



  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function (Blueprint $table) {



            $table->dropForeign(['country_id']);
            $table->dropForeign(['membership_id']);
          });


          Schema::table('country_video', function (Blueprint $table) {



              $table->dropForeign(['country_country_id']);
              $table->dropForeign(['video_video_id']);
            });


            Schema::table('session_logs', function (Blueprint $table) {

              $table->dropForeign(['user_user_id']);

            });





    }
}
