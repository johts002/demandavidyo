<?php

use Illuminate\Database\Seeder;
use App\Users;

class UsersTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

      //  DB::table('users')->delete();




        App\User::create([
            'name' => "admin",
            'email' => "admin@admin.com",
            'password' => bcrypt('admin'),

            'membership_id' => '2',
            'country_id' => '2'
        ]);

        DB::table('users')->insert([
            'name' => "Tim",
            'email' => "johts002@mymail.unisa.edu.au",
            'password' => bcrypt('johts002'),
            'country_id' => "15",
            'membership_id' => 2
        ]);

        DB::table('users')->insert([
            'name' => "Jack",
            'email' => "haljy021@mymail.unisa.edu.au",
            'password' => bcrypt('haljy021'),
            'country_id' => "15",
            'membership_id' => 2
        ]);

        DB::table('users')->insert([
            'name' => "Paul",
            'email' => "hanpj003@mymail.unisa.edu.au",
            'password' => bcrypt('hanpj003'),
            'country_id' => "15",
            'membership_id' => 2
        ]);

        DB::table('users')->insert([
            'name' => "Qianli",
            'email' => "mayqa001@mymail.unisa.edu.au",
            'password' => bcrypt('mayqa001'),
            'country_id' => "15",
            'membership_id' => 2
        ]);


        DB::table('users')->insert([
            'name' => "Australia",
            'email' => "australia@australia.com",
            'password' => bcrypt('australia'),
            'country_id' => "15",
            'membership_id' => 1
        ]);

        DB::table('users')->insert([
            'name' => "America",
            'email' => "america@america.com",
            'password' => bcrypt('america'),
            'country_id' => "1",
            'membership_id' => 1
        ]);

        DB::table('users')->insert([
            'name' => "Canada",
            'email' => "canada@canda.com",
            'password' => bcrypt('ccanada'),
            'country_id' => "2",
            'membership_id' => 1
        ]);


    }
}
