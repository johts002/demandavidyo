<?php

use Illuminate\Database\Seeder;

class MembershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //DB::table('memberships')->delete();

        DB::table('memberships')->insert([
            'name' => "Patriotic"
        ]);

        DB::table('memberships')->insert([
            'name' => "Premium"
        ]);
    }
}
