<?php

use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('events')->insert([
            'name' => "login",
            'code' => 'LI'
        ]);

        DB::table('events')->insert([
            'name' => "Watch Video",
            'code' => 'WATCH'
        ]);

        DB::table('events')->insert([
            'name' => 'Register',
            'code' => 'REG'
        ]);

        DB::table('events')->insert([
            'name' => 'Log Out',
            'code' => 'LO'
        ]);
    }
}
