<?php

use Illuminate\Database\Seeder;

use App\Video;
use App\country;

class VideoJoiner extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //




        $video = Video::find(1);

        $video->Countries()->attach(Country::find(1));
        $video->Countries()->attach(Country::find(10));
        $video->Countries()->attach(Country::find(12));
        $video->Countries()->attach(Country::find(30));
        $video->Countries()->attach(Country::where('name', 'Jamaica')->first());
        $video->Countries()->attach(Country::where('name', 'South Africa')->first());
        $video->Countries()->attach(Country::where('name', 'Germany')->first());
        $video->Countries()->attach(Country::where('name', 'Russia')->first());

        $video = Video::find(2);

        $video->Countries()->attach(Country::where('name', 'Australia')->first());
        $video->Countries()->attach(Country::where('name', 'China')->first());
        $video->Countries()->attach(Country::where('name', 'Germany')->first());
        $video->Countries()->attach(Country::where('name', 'Sweden')->first());

        $video = Video::find(3);

        $video->Countries()->attach(Country::where('name', 'United States')->first());
        $video->Countries()->attach(Country::where('name', 'China')->first());
        $video->Countries()->attach(Country::where('name', 'Germany')->first());
        $video->Countries()->attach(Country::where('name', 'Australia')->first());
        $video->Countries()->attach(Country::where('name', 'United Kingdom')->first());
        $video->Countries()->attach(Country::where('name', 'Madagascar')->first());
        $video->Countries()->attach(Country::where('name', 'Lithuania')->first());
        $video->Countries()->attach(Country::where('name', 'Russia')->first());

    }
}
