<?php

use Illuminate\Database\Seeder;
use App\Video;
use Carbon\Carbon;

class VideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /*
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('video_id');
            $table->timestamps();

            $table->string('location');

            $table->string('title');
            $table->string('description');
            $table->dateTime('release_date');

        });
        */


      //  DB::table('videos')->delete();

        Video::create([
          'location' => '/',

          'title' => '4 x 400m Relay Female Heat 1',
          'description' => 'This is a description',
          'release_date' => "2018-10-18 13:57:34",

          'thumbnail' => '4-x-400m-Relay-Female-Heat-1'
        ]);

        Video::create([
          'location' => '/',

          'title' => ' Cycling Track, Team Pursuit Men Heat 2',
          'description' => 'This is another description',
          'release_date' => "2018-10-17 13:57:34",

          'thumbnail' => 'Cycling-Track-Team-Pursuit-Men-Heat-2'
        ]);

        Video::create([
          'location' => '/',

          'title' => ' Swimming, 100m Backstroke Men Final',
          'description' => 'This is another description',
          'release_date' => "2018-10-19 13:57:34",
        ]);




    }
}
